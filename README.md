<img src="/doc/pics/GPK_BME_MOGI.png">

# EthoLink
_Implemented by Soma Deme - [demesoma@gmail.com](mailto:demesoma@gmail.com)_

Framework for ethological measurements

! Windows tested package: import winsound !


## Measurements
[MoCap measurements](https://drive.google.com/drive/folders/1lQeEloyUibZBLYOo_FJfhy4wZXmpqf2c?usp=sharing)


# Quick setup guide for EthoLink
1. Clone this repository

    `git clone https://gitlab.com/microlab1/private-projects/etorobotics/etofilter.git`
    
2. Clone the Mocapy repository either into your site-packages folder or into `EthoLink/src` (or `etofilter/src`) 

    `git clone https://gitlab.com/microlab1/private-projects/etorobotics/mocapy.git`

3. Create a folder in `EthoLink/src` (or `etofilter/src`) called `measurements`
4. Run [main.py](/src/main.py)
5. Select source (bottom left)
	- *CSV (Select):* By pressing this button, you can select your source CSV-file in an open file dialog
	- *Stream:* Gets data from MoCap stream
6. *Optional:* Select destination (bottom right)
	- *CSV:* Exports measurement to .../measurements in CSV format
	- *Matlab:* Sends processed data to Matlab via UDP communication
7. Press Play button

At this point the measurement should be playing in the main window


# Manuals
For more information, read [Mocapy manual](https://gitlab.com/microlab1/private-projects/etorobotics/mocapy/-/blob/master/doc/Manual.md) and [EthoLink manual](/doc/Manual.md).


# ToDo List
**CSV-generation**
- [x] CSV-generálás
    - [x] Új CSV-struktúra kitalálása: egyszerűbb, mint a MoCap által generált
    - [x] A generált CSV tartalma: az Etofilter által használt/kiszámolt összes változó
    - [x] Episode és config infók a generált CSV-be is mentődjenek bele
- [x] Episode folyamatos változó (timestamp helyett)
- [x] A program vissza tudja játszatni a saját maga által generált CSV-t
    - [x] A generált CSV visszanézésénél a változókat nem számoljuk újra, csak megjelenítjük
    - [x] Automatikus config load a header részbe mentett configból

**Config**
- [x] **PRIO** Episode configfile szintaktikájának kitalálása
- [x] **PRIO** Episode file load
- [x] **PRIO** Door position config (WAND rotation point)
- [x] Config file kiegészítése: size (dog, human, toy), door_origin
- [x] Config file default név: mentésnél nem muszáj megadni sajátot
- [x] Chair positioning

**GUI**
- [x] **PRIO** Play button
- [x] Settings pop up windows
    - [x] Quick config window
    - [x] IP-config window
- [x] Új főablak: a mérés során megjelenő ablak
- [x] A szereplők méretekkel rendelkeznek, kirajzolásnál ezt figyelembe venni
- [x] Measurement information
- [ ] Progress bar
- [x] Guide measurement button -> switches the sound and counter window on/off

**Filter**
- [x] who_has_toy() upgrade
    - [x] Egyszerre csak egy birtokosa lehet a labdának: ha többen jelentkeznek a labdabirtoklásra, azzé a labda, aki előbb megszerezte
- [x] is_in_room( ) upgrade
    - [x] A szereplők ne ugráljanak ki-be a belépésnél --> szebb tranziens
    - [x] Ha egy szereplő kijön, de a rigid body-ja bent ragad, akkor bizonyos idő elteltével távolítsuk el a szobából

**Data-flow**
- [x] Make stream-to-matlab work again
- [x] Make stream-from-stream work again
- [x] Streaming to Matlab and logging to CSV should be possible to be run at the same time

**Timing**
- [ ] Make playback speed real-time except in CSV-to-CSV mode
- [ ] When reading CSV, the user can jump between frames using the progress bar
- [x] Video capture starts automaticly at the beginning of the measurement

**Bugs**
- There may be problems closing the application sometimes
- If you choose 'From MoCap stream', and then stop it, later you won't be able to recieve MoCap stream unless you restart the program

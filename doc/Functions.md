# ETHOLINK - Functions

_Implemented by Soma Deme - [demesoma@gmail.com](mailto:demesoma@gmail.com)_

Here you can read most of the functions of the EthoLink class. The basic functions should be enough to use this class, however, there are some advanced functions that have been documented as well. Please, read [EthoLink - Manual](/doc/etofilter_manual.md) for more information on getting started.

## Table of Contents
[[_TOC_]]

## Basic functions

### `new_window(text="")`
Creates and configures a `toplevel` window containing GUI for Ainsworth measurements
- `text`: (string) A string to be written on the title bar of the new window

-------
### `window_close()`
Closes the window containing GUI for Ainsworth measurements

-------
### `stream_from_queue(fps_max=20)`
Does the main tasks of the class in an endless cycle: pops a `Frame` from `self.queue`, processes it, then distributes the processed data.
- `fps_max`: (float) The upper limit of `Frame`s to be processed in a second

-------
### `stream_stop()`
Stops the processing of data

-------

## Advanced functions

### `draw_room(chair_x=[], chair_y=[], chair_w=0.5)`
Draws a `self.room_width` x `self.room_height` room with chairs and a door on the main display
- `chair_x`: (list of floats) List of the X coordinates of the chairs
- `chair_y`: (list of floats) List of the Y coordinates of the chairs

-------
### `plot(x, y)`
Draws an `x`-`y` graph on `self.graph` (accepts boolean values too)
- `x`: (list of floats) List of the X values
- `y`: (list of floats) List of the Y values

-------
### `time_now = min_delay_wait(min_delay, time_last)`
If the process between `time_last` and `time_now` took shorter than `min_delay`, waits
- `time_now`: (float) The current time
- `min_delay`: (float) The minimum amount of time between `time_last` and `time_now`
- `time_last`: (float) Time before the process begun

-------
### `wand_as_top(wand_name="WAND")`
Places the top wall at the rigid body with the name `wand_name`
- `wand_name`: (string) Name of the rigid body (wand)

-------
### `wand_as_bottom(wand_name="WAND")`
Places the bottom wall at the rigid body with the name `wand_name`
- `wand_name`: (string) Name of the rigid body (wand)

-------
### `wand_as_left(wand_name="WAND")`
Places the left wall at the rigid body with the name `wand_name`
- `wand_name`: (string) Name of the rigid body (wand)

-------
### `wand_as_right(wand_name="WAND")`
Places the right wall at the rigid body with the name `wand_name`
- `wand_name`: (string) Name of the rigid body (wand)

-------
### `wand_as_chair(wand_name="WAND")`
Places a chair at the rigid body with the name `wand_name`
- `wand_name`: (string) Name of the rigid body (wand)

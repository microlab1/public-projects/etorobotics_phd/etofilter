# ETHOLINK - Manual

_Implemented by Soma Deme - [demesoma@gmail.com](mailto:demesoma@gmail.com)_

EtoFilter makes Ainsworth-test data-flow possible. It was written to select and process data coming from Mocapy then distribute it to dog behavior models.

EthoLink heavily relies on `mocapy.py`. For more information on the functions using that class, read [Mocapy - Manual](https://gitlab.com/microlab1/private-projects/mocapy/-/blob/master/doc/mocapy_manual.md).

## Table of Contents
[[_TOC_]]


## User interface

Every time a new measurement starts, a new window appears containing the graphical user interface (GUI).

![](/doc/pics/window.PNG)
<br>***Figure:** GUI present while processing*<br>

This GUI can be broken down into smaller segments:
- *Main display:* This shows the current state of the room and the participants in the middle of the window.
- *Visibility:* On the left side of the main display there are checkboxes that determine if a particular rigid body should show up in the main display.
- *Options:* At the right, you can set some parameters of the room with the use of entry fields or buttons.
- *Graph:* The graph at the bottom plots a variable (that can be chosen at the left side of the graph). This can be handy when debugging or tweaking the parameters of functions.


## Data processing

The motor of this class is `EtoFilter.stream_from_queue()`. After initializing, it makes the following steps in an endless cycle:

1. Pops a `Frame` from `EtoFilter.queue`. (This `Queue` should be the same as the one that Mocapy puts data into.)
2. Gets the necessary data from this `Frame`.
3. Calculates derived data
4. Converts the data according to the chosen ethological model.
5. Sends the converted data to the proper ethological model.
6. Draws the current state to the main display and plots the debug graph.

The cycle can be stopped using `EtoFilter.stream_stop()`.


## Options

### Visibility

With the checkboxes on the left side of the window, you can determine if a particular rigid body should show up in the main display. This way you can clear up the main display if there are too many rigid bodies in it.


### Entry fields

You can set the main parameters of the room by changing the content of the entry fields on the right side of the screen. These options are the following:

- X and Y size of the room [m]
- X and Y offset of the origin [m]
- X and Y position of each chair [m] (if you leave both entries empty, the chair will be non-existing)

In case of invalid input, the edges of these entries will become red indicating the problem. If the user gives an invalid input, a default value will be used instead of the given one.


### WAND actions

There is a faster and more sophisticated way of setting the parameters above. If you define a rigid body called 'WAND' in Motive, you can use that to show EthoLink some specific points in the room:

1. Point the previously defined wand to a position you would like to show
2. Click one of the WAND buttons on the right side of the window, so the program will behave accordingly:
    - **TOP:** Places the top wall at the WAND
    - **BOTTOM:** Places the bottom wall at the WAND
    - **LEFT:** Places the left wall at the WAND
    - **RIGHT:** Places the right wall at the WAND
    - **CHAIR:** Places a chair wall at the WAND. Note that every time you press this button, another chair will pop up at the place you desire, until you reach the maximum amount of chairs in the room. Then it will replace the first chair 


## Functions

For detailed descriptions of the functions of this class, please read [EthoLink - Functions](/doc/etofilter_functions.md).
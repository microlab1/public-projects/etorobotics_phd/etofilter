import time
from datetime import datetime

def min_delay_wait(min_delay, time_last):
    """If the process between `time_last` and the current time took shorter than `min_delay`, waits"""

    time_now = time.time()
    if time_now - time_last < min_delay:
        time.sleep(min_delay - (time_now - time_last))
    # Return current time (write it into time_last!)
    return time_now


def time_format(time, digits=0, hours_needed=False):
    """Returns a string of the *time* in the classic time format"""

    # Formatting
    if digits > 0:
        format_string = "%02i:%0" + str(digits+3) + "." + str(digits) + "f"
    else:
        format_string = "%02i:%02i"

    time = round(time, digits)
    seconds = time % 60

    if hours_needed:
        hours = int(time / 3600) % 24
        minutes = int(time / 60) % 60
        format_string = "%02i:" + format_string
        return format_string % (hours, minutes, seconds)
    else:
        minutes = int(time / 60)
        return format_string % (minutes, seconds)


def getDate():
    """Returns a string containing the current date and time"""

    return str(datetime.date(datetime.now())) + "_" + time_format(time.time(), hours_needed=True).replace(":", "-")
from tkinter import ttk
#import numpy as np
from datetime import datetime
from math import sqrt, pi, sin, cos, asin, atan2


from gui_dark import *
from mocapy.src.mocapy import *
from EthoModules import *
from camhandler import CamHandler


class EthoLink:

    # ========================================= INIT =========================================

    def __init__(self, root=None, queue=None, mocap=None, socket=None, address=None, room_width=ROOM_X_WIDTH_M,
                 room_height=ROOM_Y_WIDTH_M, origin_x=0, origin_y=0):
        """Init function for EtoFilter"""

        # GUI
        self.root = root
        self.canvas = None
        self.graph = Graph(length=100)

        # Mocap
        self.mocap = mocap

        # Camera
        self.cam = CamHandler("CAMTEST")

        # Window frames (that need to be tracked)
        self.frame_check = None
        self.frame_control = None

        # Entries for options
        self.entry_room = []
        self.entry_origin = []
        self.entry_chairs = []
        self.entry_dog_r = None
        self.entry_human_r = None

        # Room parameters
        self.room_width = room_width
        self.room_height = room_height
        self.origin_x = origin_x
        self.origin_y = origin_y
        self.i_chair = 0        # Index of selected chair

        # Frame
        self.frame = EthoFrame(self)

        # Episodes
        self.episode = Episode(self)

        # Description
        self.measurement_name = "Unknown"
        self.measurement_description = StringVar()
        self.measurement_description.set("Time: Unknown\nEpisode: Unknown\nTime left: Unknown")

        # Data flow
        self.queue = queue          # This is where we get the frames from
        self.socket = socket        # Socket of the UDP communication
        self.address = address      # Address of the destination of the UDP communication
        self.csv = CSV(self)        # CSV module
        # Streaming (terminate flag)
        self.streaming = False      # Terminate flag: When set to True, the process needs to stop
        self.stopped = False        # Flag to tell if the streaming has stopped successfully

        # Source & Destination
        self.source = IntVar()      # Source: 0 --> No source;  1 --> CSV;  2 --> Stream
        self.source.set(0)
        self.dest_csv = IntVar()    # 0: --> CSV logging is off;  1 --> CSV logging is on
        self.dest_csv.set(0)
        self.dest_matlab = IntVar() # 0: --> Don't send frames to the Matlab model;  1 --> Send frames to the Matlab model
        self.dest_matlab.set(0)

        self.measuring = False

        # Finally create a new window and start our main function
        self.window_open("EthoLink")
        self.thread_main = Thread(target=self.process_frames)
        self.thread_main.start()

    # ========================================= GUI =========================================

    def window_open(self, text=""):
        """Creates and configures a window containing GUI for Ainsworth measurements"""

        # Create root in another window
        #self.root = Toplevel()
        self.root.title(text)
        self.root.configure(bg=c_back)
        self.root.iconbitmap('m2m.ico')
        self.root.protocol("WM_DELETE_WINDOW", lambda: self.window_close())

        # Configure the grid
        Grid.rowconfigure(self.root, 0, weight=1)
        Grid.columnconfigure(self.root, 1, weight=1)

        # Measurement frame
        frame_measurement = LabelFrame(self.root, bg=c_back, bd=0, padx=15, pady=15)
        frame_measurement.grid(row=0, column=0, sticky=N+W)
        DarkLabel(frame_measurement, text="Measurement", font=("Arial Bold", 14)).pack(anchor=W)
        self.measname_entry = DarkEntry(frame_measurement, init=self.measurement_name, width=22);     self.measname_entry.pack(anchor=W)
        DarkLabel(frame_measurement, text="", textvariable=self.measurement_description, justify="left").pack(anchor=W)

        # Check frame
        DarkLabel(frame_measurement, text="\nRigid bodies to show:").pack(anchor=W)
        self.frame_check = LabelFrame(frame_measurement, bg=c_back, bd=0, padx=0, pady=0)
        self.frame_check.pack(anchor=W)

        # Create canvas
        self.canvas = Canvas(self.root, width=500, height=400, bg=c_butt, highlightthickness=0)
        self.canvas.grid(row=0, column=1, sticky=N + S + E + W)

        # Create canvas options frame
        frame_options = LabelFrame(self.root, bg=c_back, bd=0, padx=15, pady=15)
        frame_options.grid(row=0, column=2, sticky=N + W)
        DarkLabel(frame_options, text="Options", font=("Arial Bold", 14)).pack(anchor=W)
        # Config file
        DarkLabel(frame_options, text="Config file").pack(anchor=W)
        frame_config = LabelFrame(frame_options, bg=c_back, bd=0, padx=0, pady=0)
        frame_config.pack()
        DarkButton(frame_config, "Save", lambda: self.config_save(), width=9).pack(side=LEFT, padx=2)
        DarkButton(frame_config, "Load", lambda: self.config_load(), width=9).pack(side=LEFT, padx=2)
        DarkButton(frame_options, "Episodes", lambda: self.episodes_load(), width=19).pack(padx=2, pady=4)
        # Room size
        DarkLabel(frame_options, text="\nRoom size:").pack(anchor=W)
        frame_room = LabelFrame(frame_options, bg=c_back, bd=0, padx=0, pady=0)
        frame_room.pack()
        self.entry_room = []
        DarkLabel(frame_room, text="X:").grid(row=0, column=0)
        self.entry_room.append(DarkEntry(frame_room))
        DarkLabel(frame_room, text="  Y:").grid(row=0, column=2)
        self.entry_room.append(DarkEntry(frame_room))
        for i in range(len(self.entry_room)):
            self.entry_room[i].grid(row=0, column=i * 2 + 1)
        # Origin offset
        DarkLabel(frame_options, text="\nOrigin offset:").pack(anchor=W)
        frame_origin = LabelFrame(frame_options, bg=c_back, bd=0, padx=0, pady=0)
        frame_origin.pack()
        self.entry_origin = []
        DarkLabel(frame_origin, text="X:").grid(row=0, column=0)
        self.entry_origin.append(DarkEntry(frame_origin))
        DarkLabel(frame_origin, text="  Y:").grid(row=0, column=2)
        self.entry_origin.append(DarkEntry(frame_origin))
        for i in range(len(self.entry_origin)):
            self.entry_origin[i].grid(row=0, column=i * 2 + 1)
        # Chairs
        DarkLabel(frame_options, text="\nChairs:").pack(anchor=W)
        frame_chairs = LabelFrame(frame_options, bg=c_back, bd=0, padx=0, pady=0)
        frame_chairs.pack()
        self.entry_chairs = []
        for i in range(CHAIRS_MAX):  # Create entries
            self.entry_chairs.append([DarkEntry(frame_chairs), DarkEntry(frame_chairs)])
        for i in range(CHAIRS_MAX):  # Fill the grid
            DarkLabel(frame_chairs, text="X:").grid(row=i, column=0)
            self.entry_chairs[i][0].grid(row=i, column=1)
            DarkLabel(frame_chairs, text="  Y:").grid(row=i, column=2)
            self.entry_chairs[i][1].grid(row=i, column=3)
        # Participant sizes
        DarkLabel(frame_options, text="\nRadiuses of participants:").pack(anchor=W)
        frame_sizes = LabelFrame(frame_options, bg=c_back, bd=0, padx=0, pady=0)
        frame_sizes.pack()
        DarkLabel(frame_sizes, text="DOG:").grid(row=0, column=0, sticky=W, padx=5)
        DarkLabel(frame_sizes, text="Humans:").grid(row=0, column=1, sticky=W, padx=5)
        self.entry_dog_r = DarkEntry(frame_sizes)
        self.entry_dog_r.grid(row=1, column=0, sticky=W, padx=5)
        self.entry_human_r = DarkEntry(frame_sizes)
        self.entry_human_r.grid(row=1, column=1, sticky=W, padx=5)

        # Quick config
        DarkLabel(frame_options, text="\nConfigure the scene").pack(anchor=W)
        DarkButton(frame_options, "Quick config", self.quick_config_popup, None).pack(pady=2)

        # Create graph select frame
        frame_graph_select = LabelFrame(self.root, bg=c_back, bd=0, padx=15, pady=15)
        frame_graph_select.grid(row=1, column=0, sticky=N + W)
        DarkLabel(frame_graph_select, text="Graph:").pack(anchor=W)
        drop = ttk.Combobox(frame_graph_select, textvariable=self.graph.select_string)
        drop['values'] = self.graph.select_list
        drop.pack(anchor=W)

        # Create graph
        self.graph.canvas = Canvas(self.root, width=400, height=70, bg=c_butt, highlightthickness=0)
        self.graph.canvas.grid(row=1, column=1, sticky=E + W)

        # Init entries
        self.entry_room[0].set(str(self.room_width))
        self.entry_room[1].set(str(self.room_height))
        self.entry_origin[0].set(str(self.origin_x))
        self.entry_origin[1].set(str(self.origin_y))
        self.entry_dog_r.set("0.25")
        self.entry_human_r.set("0.2")

        # Source
        frame_source = DarkLabelFrame(self.root, padx=15, pady=15)
        frame_source.grid(row=2, column=0, sticky=N + S + E + W)
        DarkLabel(frame_source, "Select source").grid(row=0, sticky=W+N)
        DarkRadioButton(frame_source, self.source, 1, " CSV (Select) ", command=self.csv.load, font=("Arial Bold", 10), width=17) \
            .grid(row=1, column=0, sticky=N + S + E + W)
        DarkRadioButton(frame_source, self.source, 2, " Stream ", command=None, font=("Arial Bold", 10), width=17) \
            .grid(row=2, column=0, sticky=N + S + E + W)

        # Control panel
        self.frame_control = DarkLabelFrame(self.root, padx=0, pady=0);             self.frame_control.grid(row=2, column=1)
        frame_control_left = DarkLabelFrame(self.frame_control, padx=0, pady=0);    frame_control_left.grid(row=0, column=1)
        DarkButton(frame_control_left, "Counter display", lambda: self.episode.window_open(), lambda: self.episode.window_close(),
                   text_active="Counter display", color_active=c_clck).pack(pady=3)
        DarkButton(frame_control_left, "Camera", lambda: self.cam.start(), lambda: self.cam.stop(),
                   text_active="Camera", color_active=c_clck).pack(pady=3)
        DarkButton(self.frame_control, "4", lambda: self.measurement_start(), lambda: self.measurement_stop(),
                   width=2, height=1, font=("Webdings", 21), color_active=c_green, text_active=";").grid(row=0, column=2, padx=20)
        DarkButton(self.frame_control, "IP config", lambda: self.ip_config_popup()).grid(row=0, column=3)

        # Destination
        frame_destination = DarkLabelFrame(self.root, padx=15, pady=15)
        frame_destination.grid(row=2, column=2, sticky=N + S + E + W)
        DarkLabel(frame_destination, "Select destination").grid(row=0, sticky=W + N)
        DarkButton(frame_destination, " CSV ", text_active=" CSV ", color_active=c_clck, color_text=c_clck, lambda_default=lambda: self.dest_csv.set(1),
                   lambda_active=lambda: self.dest_csv.set(0), font=("Arial Bold", 10), width=17, pady=0).grid(row=1, column=0, sticky=N + S + E + W)
        DarkButton(frame_destination, " Matlab ", text_active=" Matlab ", color_active=c_clck, color_text=c_clck, lambda_default=lambda: self.dest_matlab.set(1),
                   lambda_active=lambda: self.dest_matlab.set(0), font=("Arial Bold", 10), width=17, pady=0).grid(row=2, column=0, sticky=N + S + E + W)


    def window_close(self):
        """Closes the window containing GUI for Ainsworth measurements"""

        # Start closing
        self.streaming = False

        # Wait for process_frames() to close
        while not self.stopped:
            self.root.update()

        # Close window
        self.root.destroy()


    def description_update(self):
        """Updates the measurement description in the GUI"""

        self.measurement_name = self.measname_entry.get()
        self.measurement_description.set("\nTime: " + time_format(self.frame.time, 0, True) + "\n"
                                         + self.episode.text.get() + "\nTime left: " + time_format(self.episode.time_left)
                                         + "\nEpisode state: " + str(self.episode.number))


    def ip_config_popup(self):
        """Opens a pop-up window, where the user can set the IP-configurations"""

        # Create a popup window
        popup = Toplevel()
        popup.title("IP config")
        popup.grab_set()
        popup.configure(bg=c_back)
        popup.iconbitmap('m2m.ico')
        popup.configure(padx=10, pady=10)
        popup.resizable(0, 0)

        # Create entries for new values
        DarkLabel(popup, "Server (Motive) IP address").pack(anchor=W)
        entry_server_ip = DarkEntry(popup, width=16, init=self.mocap.server_address_get())
        entry_server_ip.pack(anchor=W)
        DarkLabel(popup, "\nClient (Matlab) IP address").pack(anchor=W)
        entry_send_ip = DarkEntry(popup, width=16, init=self.address[0])
        entry_send_ip.pack(anchor=W)
        DarkLabel(popup, "Client (Matlab) port").pack(anchor=W)
        entry_send_port = DarkEntry(popup, init=self.address[1])
        entry_send_port.pack(anchor=W)

        # Create function for OK button
        def set_values():
            tmp1 = entry_server_ip.get_ip(default=self.mocap.server_address_get())
            tmp2 = entry_send_ip.get_ip(default=self.address[0])
            tmp3 = entry_send_port.get_number(integer=True, nonnegative=True, default=self.address[1])

            # Check if the inputs are valid, if so, update values
            if entry_server_ip.is_valid and entry_send_ip.is_valid and entry_send_port.is_valid:
                self.mocap.server_address_set(tmp1)
                self.address = (tmp2, tmp3)

                print("IP config changed")
                popup.destroy()

        DarkButton(popup, "OK", lambda: set_values()).pack(pady=5)


    def quick_config_popup(self):
        """Opens a pop-up window, where the user can quick-config the scene"""

        # Create a popup window
        popup = Toplevel()
        popup.title("Quick config")
        popup.grab_set()
        popup.configure(bg=c_back)
        popup.iconbitmap('m2m.ico')
        popup.configure(padx=10, pady=10)
        popup.resizable(0, 0)

        # WAND buttons
        DarkLabel(popup, text="\nSet WAND position as...").pack(anchor=W)
        DarkButton(popup, "TOP", self.wand_as_top, None).pack(pady=2)
        DarkButton(popup, "BOTTOM", self.wand_as_bottom, None).pack(pady=2)
        DarkButton(popup, "LEFT", self.wand_as_left, None).pack(pady=2)
        DarkButton(popup, "RIGHT", self.wand_as_right, None).pack(pady=2)
        DarkButton(popup, "CHAIR", self.wand_as_chair, None).pack(pady=2)
        DarkButton(popup, "DOOR", self.wand_as_door_origin, None).pack(pady=2)

        # DOOR button
        DarkLabel(popup, text="\nSet this as...").pack(anchor=W)
        DarkButton(popup, "DOOR CLOSED", self.set_door_closed, None).pack(pady=2)

    # ========================================= DRAW =========================================

    def draw_room(self, chair_x=[], chair_y=[], chair_w=0.5):
        """Draws a `self.room_width` x `self.room_height` room with chairs and a door on the main display."""

        # If canvas is None, return
        if not self.canvas:
            print("No canvas to draw on!")
            return

        # Calculate coordinate conversions (multiplier, offsets)
        canvas_width = self.canvas.winfo_width()
        canvas_height = self.canvas.winfo_height()

        # Multiplier (m to pixel)
        if canvas_width / canvas_height > self.room_width / self.room_height:
            conv_mult = 0.95 * canvas_height / self.room_height
        else:
            conv_mult = 0.95 * canvas_width / self.room_width

        # room_offset (room's left bottom coordinate [pixel])
        room_offset_x = (canvas_width - self.room_width * conv_mult) / 2
        room_offset_y = (canvas_height + self.room_height * conv_mult) / 2

        # Draw door
        door_r = self.frame.door.len * conv_mult
        door_x = (self.frame.door.x + self.origin_x) * conv_mult
        if self.canvas.find_withtag("door"):  # If the item already exists
            self.canvas.coords("door", room_offset_x + door_x - door_r, room_offset_y - door_r,
                                       room_offset_x + door_x + door_r, room_offset_y + door_r)

        else:  # Else, create a new one
            self.canvas.create_arc(room_offset_x + door_x  - door_r, room_offset_y - door_r,
                                   room_offset_x + door_x  + door_r, room_offset_y + door_r,
                              start=90, extent=90,
                              outline="red", fill='#700', tags="door")

        # Draw chairs
        chair_w = chair_w * conv_mult
        for i in range(len(chair_x)):
            x = (chair_x[i] + self.origin_x) * conv_mult + room_offset_x
            y = - (chair_y[i] + self.origin_y) * conv_mult + room_offset_y
            tag = "chair_" + str(i)
            if self.canvas.find_withtag(tag):  # If the item already exists
                self.canvas.coords(tag, x - chair_w / 2, y - chair_w / 2,
                              x + chair_w / 2, y + chair_w / 2)

            else:  # Else, create a new one
                self.canvas.create_rectangle(x - chair_w / 2, y - chair_w / 2,
                                        x + chair_w / 2, y + chair_w / 2,
                                        outline='#BD5100', fill='#7C3500', tags=tag)
                self.canvas.lower(tag)

        # Delete leftover chairs
        for i in range(len(chair_x), CHAIRS_MAX):
            self.canvas.delete("chair_" + str(i))


    def draw_bar(self, canvas, frame_current, frame_max):
        """Draws a progress bar on a canvas"""

        # If canvas is None, return
        if not canvas:
            print("No canvas to draw on!")
            return

        # Calculate coordinate conversions (multiplier, offsets)
        canvas_width = canvas.winfo_width()
        canvas_height = canvas.winfo_height()

        # Multiplier (m to pixel)
        if canvas_width / canvas_height > ROOM_X_WIDTH_M / ROOM_Y_WIDTH_M:
            conv_mult = 0.95 * canvas_height / ROOM_Y_WIDTH_M
        else:
            conv_mult = 0.95 * canvas_width / ROOM_X_WIDTH_M

        # Draw bar
        if canvas.find_withtag("bar"):  # If the item already exists
            canvas.coords("bar", 0, canvas_height,
                          canvas_width * (frame_current / frame_max), canvas_height - 9)

        else:  # Else, create a new one
            canvas.create_rectangle(0, canvas_height,
                                    canvas_width * (frame_current / frame_max), canvas_height - 9,
                                    outline="green", fill="green", tags="bar")


    def plot(self, x, y):
        """Draws an "*x-y*" graph on self.canv_graph"""

        # Calculate coordinate conversions (multiplier, offsets)
        # Canvas sizes and borders
        canvas_width = self.graph.canvas.winfo_width()
        canvas_height = self.graph.canvas.winfo_height()
        border = canvas_height / 4
        bottom = 1.2 * border

        # Min-max values of the lists. If y is binary, we know the min-max of it
        x_min = min(x)
        x_max = max(x)
        if type(y[0]) is not bool:
            y_min = min(y)
            y_max = max(y)
        else:
            y_min = False
            y_max = True

        if x_max != x_min:
            mult_x = (canvas_width - 2 * border) / (x_max - x_min)
        else:
            mult_x = 1
        if y_max != y_min:
            mult_y = (canvas_height - (border + bottom)) / (y_max - y_min)
        else:
            mult_y = 1

        # Draw lines
        for i in range(len(x) - 1):
            # Coordinates
            x_1 = border + (x[i] - x_min) * mult_x
            x_2 = border + (x[i + 1] - x_min) * mult_x
            y_1 = - (bottom + (y[i] - y_min) * mult_y) + canvas_height
            y_2 = - (bottom + (y[i + 1] - y_min) * mult_y) + canvas_height

            # Tag
            tag = "line_" + str(i)

            if self.graph.canvas.find_withtag(tag):  # If the item already exists
                self.graph.canvas.coords(tag, x_1, y_1, x_2, y_2)

            else:  # Else, create a new one
                self.graph.canvas.create_line(x_1, y_1, x_2, y_2, fill='blue', width=2, tags=tag)

        # Draw text
        tag_min = "text_min"
        text_min = time_format(x_min, 1, True)
        tag_max = "text_max"
        text_max = time_format(x_max, 1, True)
        if self.graph.canvas.find_withtag(tag_min):  # If the items already exist
            self.graph.canvas.itemconfig(tag_min, text=text_min)
            self.graph.canvas.itemconfig(tag_max, text=text_max)

        else:  # Else, create new ones
            self.graph.canvas.create_text(5, canvas_height - 5, text=text_min, tags=tag_min, anchor=SW, fill="white")
            self.graph.canvas.create_text(canvas_width - 5, canvas_height - 5, text=text_max, tags=tag_max, anchor=SE,
                                   fill="white")

    # ========================================= STREAM =========================================

    def process_frames(self, fps_max=20):
        """Does the main tasks of the class in an endless cycle: pops a `Frame` from `self.queue`, processes it, then distributes the processed data.
        `fps_max`: (float) The upper limit of `Frame`s to be processed in a second"""

        # ------------------------------ INIT ------------------------------

        # TODO: Stream init függvény létrehozása, az itt "nullázott változók használhatóak az EtoFilter.init()-nél is"
        # Streaming
        self.streaming = True
        self.stopped = False

        time_last = 0
        t_draw = None
        t_graph = None
        self.i_chair = 0

        # Creating lists for plotting
        self.graph.init()

        # ------------------------------ STREAMING ------------------------------

        while True:
            while self.queue.empty():
                time.sleep(0.05)  # In case of empty queue, wait

                # If streaming is False, exit
                if not self.streaming and (t_graph is None or not t_graph.is_alive()) \
                                      and (t_draw is None or not t_draw.is_alive()) \
                   or (self.csv.is_ecsv and self.measuring):
                    break

            # If streaming is False, exit
            if not self.streaming and (t_graph is None or not t_graph.is_alive()) \
                                  and (t_draw is None or not t_draw.is_alive()):
                print("Stopping process_frames()")
                break

            # Popping a frame from queue
            if self.csv.is_ecsv:
                self.csv.ecsv.read_line()
            else:
                self.frame.get_data(self.queue.get())

            # Reading room parameters from entries
            # TODO: Is there a way to do this in an "entry modified" event?
            self.room_width = self.entry_room[0].get_number(default=1, nonnegative=True, nonzero=True)
            self.room_height = self.entry_room[1].get_number(default=1, nonnegative=True, nonzero=True)
            self.origin_x = self.entry_origin[0].get_number()
            self.origin_y = self.entry_origin[1].get_number()
            self.frame.dog.r = self.entry_dog_r.get_number(default=0.4, nonnegative=True, nonzero=True)
            self.frame.own.r = self.frame.str.r = self.entry_human_r.get_number(default=0.4, nonnegative=True, nonzero=True)

            if not self.csv.is_ecsv:
                # Calculate additional values
                self.who_is_in_room()
                self.who_has_toy()
                self.is_door_open()

                # Estimate TOY's position if needed
                self.estimate_toy_pos()

                # Update timer
                self.episode.timer()

            # Update description in GUI
            self.description_update()

            # Update graph values
            self.graph.update(self.frame)

            # Send/export data if needed
            if self.measuring:
                if self.dest_csv.get() == 1:
                    self.csv.write_line()
                if self.dest_matlab.get() == 1:
                    self.matlab_send()

            # ------------------------------ Drawing ------------------------------
            # Draw scene
            if (t_draw is None or not t_draw.is_alive()) and self.streaming:
                # Reading chair coordinates from entries
                chair_x, chair_y = self.entries_to_lists(self.entry_chairs)
                # Init and start draw thread
                t_draw = Thread(target=lambda: [self.draw_room(chair_x, chair_y),
                                                self.frame.draw(self.canvas, self.room_width, self.room_height,
                                                                self.origin_x, self.origin_y, draw_markers=False,
                                                                frame_check=self.frame_check)])
                t_draw.start()

            # Draw graph
            if (t_graph is None or not t_graph.is_alive()) and self.streaming:
                # Select the graph needed
                graph_y = self.graph.select_y()

                # If needed, init and start graph thread
                if self.graph.is_draw_needed:
                    t_graph = Thread(target=lambda: [self.plot(deepcopy(self.graph.time), deepcopy(graph_y))])
                    t_graph.start()
                else:  # Else, clear graph
                    self.graph.canvas.delete("all")

            # If it's too early to send the next package, wait
            time_last = min_delay_wait(1/fps_max, time_last)

        print("process_frames() has stopped")
        self.stopped = True


    def process_frames_stop(self):
        """Stops the processing of data"""
        self.streaming = False

    # ========================================= MEASUREMENT =========================================

    def measurement_start(self):
        """Starts the measurement. This includes streaming/logging if needed and episode timing."""

        # Update measurement name
        self.measurement_name = self.measname_entry.get()

        # Start recording if camera is on
        self.cam.setName("measurements/" + self.measurement_name + "_" + getDate())
        self.cam.recStart()

        # Reset door was open flag
        self.frame.door.was_open = False

        if self.source.get() == 0:
            return

        self.measuring = True
        self.episode.started = self.frame.time

        # Start reading from source
        if self.source.get() == 1:
            self.csv.read_start()
        elif self.source.get() == 2:
            self.mocap.natnet_start()

        # Write header for CSV
        if self.dest_csv.get() == 1:
            self.csv.write_header()


    def measurement_stop(self):
        """Stops the measurement"""

        if self.source.get() == 1 and not self.csv.is_ecsv:
            self.csv.read_stop()
        elif self.source.get() == 2:
            self.mocap.natnet_stop()

        self.measuring = False
        # Episodes TODO: replace this with an init function
        self.episode.number = 0.5  # Current episode. Initialising with 0.5 episode 0 is over when the the door closes
        self.episode.started = 0

        # Stop recording
        self.cam.recStop()

    # ========================================= ETO FUNCTIONS =========================================

    def who_is_in_room(self, dist_max=1, t_off_door=3, t_off_inside=10):
        """Returns if *entity* is in the room."""

        # Get coordinates of walls
        w_left = -self.origin_x
        w_right = self.room_width - self.origin_x
        w_bottom = -self.origin_y
        w_top = self.room_height - self.origin_y

        for rb in self.frame.participants_is_in_room:

            # If rb is outside then it is outside
            if rb.position.x < w_left - dist_max or rb.position.x > w_right + dist_max or \
                    rb.position.y < w_bottom - dist_max or rb.position.y > w_top + dist_max:
                rb.is_in_room = False

            # Else if rb is tracked then it is inside
            elif rb.tracked:
                rb.is_in_room = True

            # Else decide rb.is_in_room based on rb.last_seen
            else:
                # Get the correct t_off value
                if sqrt((rb.position.x - self.frame.door.x) ** 2 + (rb.position.y - w_bottom) ** 2) < self.frame.door.len:
                    t_off = t_off_door
                else:
                    t_off = t_off_inside

                # If the rigidBody is yet to be seen or was seen too long ago, it is outside
                if rb.last_seen is None or self.frame.time - rb.last_seen > t_off:
                    rb.is_in_room = False
                else:
                    rb.is_in_room = True


    def who_has_toy(self):
        # TODO: Elég lenne félgömböket checkolni -> kevesebb false positive
        """Sets .has_toy variables for the participants listed in self.frame.participants_has_toy"""

        # If the toy is yet to be seen, return
        if self.frame.toy.last_seen is None:
            return

        # Check is anyone had the toy last frame
        for part in self.frame.participants_has_toy:
            if part.has_toy:
                if (part.hand.position - self.frame.toy.position).magnitude() < part.toy_threshold:
                    # If part still has the toy, return
                    return
                else:
                    # If part doesn't have it anymore, update .has_toy
                    part.has_toy = False

        # If noone had the toy last frame
        else:
            for part in self.frame.participants_has_toy:
                if (part.hand.position - self.frame.toy.position).magnitude() < part.toy_threshold:
                    part.has_toy = True
                    return


    def is_door_open(self):
        """Checks if the door is open by comparing the closed and the current rotation of the door"""

        # If the door is tracked, its state shall be checked
        if (self.frame.door.position - self.frame.door.closed_position).magnitude() > 0.15:
            if not self.frame.door.is_open and self.measuring:
                # If the door is opening at the moment, save the time of this action
                self.frame.door.open_last = self.frame.time

            if self.episode.number != int(self.episode.number) and self.measuring:
                # Note any door openings during episode ending state
                self.frame.door.was_open = True

            self.frame.door.is_open = True

        else:
            self.frame.door.is_open = False


    def estimate_toy_pos(self):
        """Estimates TOY's position if it is not tracked based on who has it"""

        # If not tracked, assume the position
        if not self.frame.toy.tracked:
            if self.frame.dog.has_toy:
                self.frame.toy.position = self.frame.dog.position
            elif self.frame.own.has_toy:
                self.frame.toy.position = self.frame.oha.position
            elif self.frame.str.has_toy:
                self.frame.toy.position = self.frame.sha.position


    def entries_to_lists(self, entries):
        """Converts the *entries* (2D list) for chair coordinates to *chair_x* and *chair_y* lists"""

        # Init
        chair_x = []
        chair_y = []

        # Convert the existing entries
        for e in entries:
            nums = [e[0].get_number(canbeempty=True), e[1].get_number(canbeempty=True)]
            if nums[0] is None and nums[1] is None:
                pass
            else:
                if nums[0] is None:
                    nums[0] = 0
                if nums[1] is None:
                    nums[1] = 0
                chair_x.append(nums[0])
                chair_y.append(nums[1])

        return chair_x, chair_y

    # ========================================= QUICK-CONFIG =========================================

    def wand_as_top(self):
        """Places the top wall at the rigid body with the name `wand_name`"""

        self.room_height = self.origin_y + self.frame.wand.position.y
        self.entry_room[1].set(self.room_height)


    def wand_as_right(self):

        self.room_width = self.origin_x + self.frame.wand.position.x
        self.entry_room[0].set(self.room_width)


    def wand_as_bottom(self):

        self.room_height = self.room_height - self.frame.wand.position.y - self.origin_y
        self.origin_y = - self.frame.wand.position.y
        self.entry_room[1].set(self.room_height)
        self.entry_origin[1].set(self.origin_y)


    def wand_as_left(self):

        self.room_width = self.room_width - self.frame.wand.position.x - self.origin_x
        self.origin_x = - self.frame.wand.position.x
        self.entry_room[0].set(self.room_width)
        self.entry_origin[0].set(self.origin_x)


    def wand_as_chair(self):

        # Find first free chair slot and set position
        for chair in self.entry_chairs:
            if chair[0].is_empty() and chair[1].is_empty():
                chair[0].set(self.frame.wand.position.x)
                chair[1].set(self.frame.wand.position.y)
                return

        # If no free slot found, start overwriting from oldest to newest
        self.entry_chairs[self.i_chair][0].set(self.frame.wand.position.x)
        self.entry_chairs[self.i_chair][1].set(self.frame.wand.position.y)

        # Change "highlighted" chair
        if self.i_chair < CHAIRS_MAX - 1:
            self.i_chair += 1
        else:
            self. i_chair = 0


    def wand_as_door_origin(self):
        """Places the rotation point of the door where the wand is"""

        # Adjust the door's x coordinate
        self.frame.door.x = self.frame.wand.position.x

        # Adjust the bottom wall to mach the door's y coordinate
        self.wand_as_bottom()

        # Adjust door size
        # Find door
        door = self.frame.rigid_body_get("DOOR")

        self.frame.door.len = sqrt((self.frame.wand.position.x - door.position.x)**2 + (self.frame.wand.position.y - door.position.y)**2)


    def set_door_closed(self):
        """Sets the closed rotation of the door"""

        self.frame.door.closed_position = self.frame.door.position

    # ========================================= CONFIG FILES / METADATA =========================================

    def config_build(self):
        """Builds and returns the string of the config file"""

        # Room size
        string = "room_width:"
        string += str(self.room_width) + "\n"
        string += "room_height:"
        string += str(self.room_height) + "\n\n"

        # Origin
        string += "origin_offset_x:"
        string += str(self.origin_x) + "\n"
        string += "origin_offset_y:"
        string += str(self.origin_y) + "\n\n"

        # Chairs
        chairs_x, chairs_y = self.entries_to_lists(self.entry_chairs)
        string += "chairs_x:"
        for i in range(len(chairs_x)):
            string += str(chairs_x[i]) + ','
        string = string[0:-1]
        string += "\n"
        string += "chairs_y:"
        for i in range(len(chairs_y)):
            string += str(chairs_y[i]) + ','
        string = string[0:-1]
        string += "\n\n"

        # Door
        string += "DOOR_x:"
        string += str(self.frame.door.x) + "\n"
        string += "DOOR_r:"
        string += str(self.frame.door.len) + "\n"
        string += "DOOR_closed_position:"
        string += str(self.frame.door.closed_position) + "\n\n"

        # Participant radiuses
        string += "DOG_r:"
        string += str(self.frame.dog.r) + "\n"
        string += "human_r:"
        string += str(self.frame.own.r) + "\n"

        return string


    def config_save(self, file_path=""):
        """Saves the current settings in a config file. If it doesn't get a file_path, it opens a save file dialog"""

        # Save file dialog if needed
        if file_path == "":
            file_path = filedialog.asksaveasfilename(defaultextension='.txt', initialfile=self.measurement_name + "_" + getDate(),
                                                     title="Select the desired location of the config file")

        # If the dialog was closed, return
        if file_path == "":
            return

        # Build the string to write in the config file
        string = "[Eto measurement configuration file]\n"
        string += "Created: " + datetime.now().strftime("%Y/%m/%d, %H:%M:%S") + "\n\n"

        # Get config string
        string += self.config_build()

        # Write file
        with open(file_path, 'w') as f:
            f.write(string)


    def config_read(self, config_string):
        """Gets config data from *config_string*"""

        # Split string into lines
        lines = config_string.split('\n')

        for line in lines:
            data = line.split(':')

            # Room size
            if data[0] == "room_width":
                self.entry_room[0].set(data[1])
            if data[0] == "room_height":
                self.entry_room[1].set(data[1])

            # Origin
            if data[0] == "origin_offset_x":
                self.entry_origin[0].set(data[1])
            if data[0] == "origin_offset_y":
                self.entry_origin[1].set(data[1])

            # Chairs
            if data[0] == "chairs_x":
                if len(data) > 1:
                    chair = data[1].split(',')
                else:
                    chair = []
                for i in range(len(chair)):
                    self.entry_chairs[i][0].set(chair[i])
                for i in range(len(chair), CHAIRS_MAX):
                    self.entry_chairs[i][0].set("")
            if data[0] == "chairs_y":
                if len(data) > 1:
                    chair = data[1].split(',')
                else:
                    chair = []
                for i in range(len(chair)):
                    self.entry_chairs[i][1].set(chair[i])
                for i in range(len(chair), CHAIRS_MAX):
                    self.entry_chairs[i][1].set("")

            # Door
            if data[0] == "DOOR_x":
                self.frame.door.x = float(data[1])
            if data[0] == "DOOR_r":
                self.frame.door.len = float(data[1])
            if data[0] == "DOOR_closed_position":
                self.frame.door.closed_position.from_str(data[1])

            # Participant radiuses
            if data[0] == "DOG_r":
                self.entry_dog_r.set(data[1])
            if data[0] == "human_r":
                self.entry_human_r.set(data[1])

        print("Config loaded")


    def config_load(self):
        """Opens a config file and loads the parameters from it."""

        # Open file dialog
        file_path = filedialog.askopenfilename(initialdir="", title="Select a config file",
                                               filetypes=(("Text files", "*.txt"), ("All files", "*.*")))

        # If the dialog was closed, return
        if file_path == "":
            return

        # Reading file
        with open(file_path, 'r') as f:
            config_string = f.read()

        self.config_read(config_string)


    def episodes_build(self):
        """Builds and returns the episode config string"""

        content = ""
        for i in range(len(self.episode.intervals)):
            content += "{" + self.episode.names[i] + "}{" + str(self.episode.intervals[i]) + "}\n"

        return content


    def episodes_read(self, episodes_string):

        self.episode.intervals = []
        self.episode.names = []

        lines = episodes_string.split('\n')

        for line in lines:
            if line != "" and line[0] == "{":
                data = line.split('}')

                self.episode.names.append(data[0][1:])
                self.episode.intervals.append(float(data[1][1:]))

        print("Episodes loaded")


    def episodes_load(self):
        """Opens an episode config file and loads the episode lenghts/names from it."""

        # Open file dialog
        file_path = filedialog.askopenfilename(initialdir="", title="Select a config file",
                                               filetypes=(("Text files", "*.txt"), ("All files", "*.*")))

        # If the dialog was closed, return
        if file_path == "":
            return

        # Reading file
        with open(file_path, 'r') as f:
            episodes_string = f.read()

        self.episodes_read(episodes_string)


    def metadata_build(self):
        """Builds and returns the metadata string"""

        string = "Number of strings:"
        string += str(self.frame.sn) + "\n"

        return string


    def metadata_read(self, metadata_string):
        """Loads the metadata from *metadata_string*"""

        lines = metadata_string.split('\n')

        for line in lines:
            data = line.split(':')

            if data[0] == "Number of frames":
                self.num_of_frames = int(data[1])

        print("Metadata loaded")

    # ========================================= MATLAB =========================================

    def conv_16_2_8(self, num_16):
        """Converts a 16 bit integer to two 8 bit integers (higher and lower bits)"""

        num_8_hi = num_16 >> 8
        num_8_lo = num_16 - (num_8_hi << 8)
        return num_8_hi, num_8_lo


    def matlab_conv(self, position, multiplier, offset=0):
        """Converts *position* to match matlab requirements."""

        # Convert
        position = int((position + offset) * multiplier)

        # Check limits
        if position > (1 << 16) - 1:
            position = (1 << 16) - 1
        elif position < 0:
            position = 0

        return position


    def matlab_send(self):

        # Multipliers
        mult_x = float((1 << 16) - 1) / self.room_width
        mult_y = float((1 << 16) - 1) / self.room_height
        mult_circ = float((1 << 8) - 1) / 360

        # Convert 16 bit to 8 bit
        currentpacketno_hi, currentpacketno_lo = self.conv_16_2_8(self.frame.sn)

        # Get participants
        dog = self.frame.rigid_body_get("DOG")
        own = self.frame.rigid_body_get("OWN")
        str = self.frame.rigid_body_get("STR")
        toy = self.frame.rigid_body_get("TOY")

        dogposx_hi, dogposx_lo = self.conv_16_2_8(self.matlab_conv(dog.position.x, mult_x, self.origin_x))
        dogposy_hi, dogposy_lo = self.conv_16_2_8(self.matlab_conv(dog.position.y, mult_y, self.origin_y))
        dogrot = self.matlab_conv(dog.rotation.z, mult_circ)

        toyposx_hi, toyposx_lo = self.conv_16_2_8(self.matlab_conv(toy.position.x, mult_x, self.origin_x))
        toyposy_hi, toyposy_lo = self.conv_16_2_8(self.matlab_conv(toy.position.y, mult_y, self.origin_y))
        toyposz_hi, toyposz_lo = self.conv_16_2_8(self.matlab_conv(toy.position.z, mult_x))

        ownerposx_hi, ownerposx_lo = self.conv_16_2_8(self.matlab_conv(own.position.x, mult_x, self.origin_x))
        ownerposy_hi, ownerposy_lo = self.conv_16_2_8(self.matlab_conv(own.position.y, mult_y, self.origin_y))

        strangerposx_hi, strangerposx_lo = self.conv_16_2_8(self.matlab_conv(str.position.x, mult_x, self.origin_x))
        strangerposy_hi, strangerposy_lo = self.conv_16_2_8(self.matlab_conv(str.position.y, mult_y, self.origin_y))

        # Putting it in a tuple
        visdata = (currentpacketno_hi, currentpacketno_lo,
                   dogposx_hi, dogposx_lo, dogposy_hi, dogposy_lo, dogrot,
                   toyposx_hi, toyposx_lo, toyposy_hi, toyposy_lo, toyposz_hi, toyposz_lo,
                   ownerposx_hi, ownerposx_lo, ownerposy_hi, ownerposy_lo,
                   strangerposx_hi, strangerposx_lo, strangerposy_hi, strangerposy_lo,
                   own.has_toy, str.has_toy, dog.has_toy, ~own.is_in_room, ~str.is_in_room)

        visdata = np.ubyte(visdata)

        udp_send(self.socket, self.address, visdata)

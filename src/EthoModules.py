from copy import deepcopy
import socket
import winsound
from threading import Thread
import time
from tkinter import filedialog
import os

from gui_dark import *
from mocapy.src.mocapy import *
from CommonFuncs import *

# CONSTANTS

# Room size [m]
ROOM_X_WIDTH_M = 4
ROOM_Y_WIDTH_M = 5

# CHAIRS MAX
CHAIRS_MAX = 4


# ========================================= UDP =========================================

def udp_init(address):
    sckt = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sckt.bind(address)
    print("Socket ready")
    return sckt


def udp_rec(sckt):
    while True:
        data, address = sckt.recvfrom(1024)
        print(data)
        print(address)
        if data == b"kill":
            sckt.shutdown(socket.SHUT_WR)
            print("Socket closed")
            return


def udp_send(sckt, address, data):
    sckt.sendto(data, address)
    return


def udp_close(sckt, address):
    udp_send(sckt, address, b"kill")
    print("Closing...")
    return



class Episode:
    def __init__(self, parent):
        self.parent = parent
        self.number = 0.5  # Current episode. Initialising with 0.5 episode 0 is over when the the door closes
        self.started = 0
        self.time_left = 0
        self.intervals = []
        self.names = []
        self.text = StringVar()
        self.text_deb = StringVar()
        self.counter_text = StringVar()
        self.window = None

    def reset(self):
        self.number = 0.5
        self.started = 0
        self.time_left = 0
        self.text = StringVar()
        self.counter_text = StringVar()

    def timer(self):
        """Keeps track of the current episode of the measurement based on frame.time and DOOR movement.
        Signals when a new episode should start."""

        # If the measurement is over or there are no episodes or there is no ongoing measurement, return
        if int(self.number) > len(self.intervals) or self.intervals == [] or not self.parent.measuring:
            return

        # If the measurement hasn't started yet, write "START"
        if self.number == 0.5:
            self.counter_text.set("START")
            self.text.set(self.names[0])


        # Calculate remaining time
        if self.number == int(self.number):
            self.time_left = self.intervals[int(self.number) - 1] - (self.parent.frame.time - self.started)
        else:
            self.time_left = 0

        # Upgrade counter texts and episodes
        if int(self.number) == self.number:    # If the measurement is not in "episode ending" state
            if self.time_left > 0:      # If there is still time left
                self.counter_text.set(time_format(self.time_left))
            else:
                self.counter_text.set("END")
                Thread(target=lambda: winsound.PlaySound("beep", winsound.SND_FILENAME)).start()
                self.number += 0.5

        elif (not self.parent.frame.door.is_open and self.parent.frame.door.was_open and self.parent.frame.time - self.parent.frame.door.open_last > 0.3)\
                and self.number < len(self.intervals):
            self.started = self.parent.frame.time
            self.text.set(self.names[int(self.number)])
            self.parent.frame.door.was_open = False
            self.number += 0.5


    def window_open(self):
        """Opens a new window for the episode timer"""

        self.window = Toplevel()
        self.window.title("Counter")
        self.window.configure(bg=c_back)
        self.window.iconbitmap('m2m.ico')

        episode_label = DarkLabel(self.window, "", textvariable=self.text, font=("Arial Bold", 32))
        episode_label.pack(padx=30)
        self.text.set("Waiting to Start")

        counter_label = DarkLabel(self.window, "", textvariable=self.counter_text, font=("Arial Bold", 150))
        counter_label.pack(padx=30)


    def window_close(self):
        """Opens a new window for the episode timer"""

        # Close window
        self.window.destroy()
        self.window = None


class Graph:
    def __init__(self, length=100):
        # Creating lists for plotting
        self.canvas = None
        self.time = [0.0] * length
        self.toy_at_owner = [False] * length
        self.toy_at_stranger = [False] * length
        self.toy_at_dog = [False] * length
        self.door_open = [False] * length
        self.toy_tracked = [False] * length
        self.dog_tracked = [False] * length
        self.owner_tracked = [False] * length
        self.stranger_tracked = [False] * length

        # Putting the lists into a list -> easier to pop
        self.lists = [self.time, self.toy_at_owner, self.toy_at_stranger, self.toy_at_dog, self.door_open,
                      self.toy_tracked, self.dog_tracked, self.owner_tracked, self.stranger_tracked]

        self.select_list = ["None",
                            "toy_at_dog",
                            "toy_at_owner",
                            "toy_at_stranger",
                            "door_open",
                            "toy_tracked",
                            "dog_tracked",
                            "owner_tracked",
                            "stranger_tracked"]
        self.select_string = StringVar()
        self.select_string.set(self.select_list[0])

        self.is_draw_needed = False

    def init(self):
        for list in self.lists:
            for elem in list:
                if isinstance(elem, (bool)):
                    elem = False
                else:
                    elem = 0

    def update(self, frame):
        """Updates the values of the lists storing graph data"""

        # Remove oldest values from graph data
        for list in self.lists:
            list.pop()

        # Update new values
        self.time.insert(0, frame.time)
        self.toy_at_owner.insert(0, frame.own.has_toy)
        self.toy_at_stranger.insert(0, frame.str.has_toy)
        self.toy_at_dog.insert(0, frame.dog.has_toy)
        self.door_open.insert(0, frame.door.is_open)
        self.toy_tracked.insert(0, frame.toy.tracked)
        self.dog_tracked.insert(0, frame.dog.tracked)
        self.owner_tracked.insert(0, frame.own.tracked)
        self.stranger_tracked.insert(0, frame.str.tracked)

    def select_y(self):
        """Selects the Y list to plot"""

        g_sel = self.select_string.get()
        self.is_draw_needed = True
        graph_y = None

        if g_sel == "toy_at_dog":
            graph_y = self.toy_at_dog
        elif g_sel == "toy_at_owner":
            graph_y = self.toy_at_owner
        elif g_sel == "toy_at_stranger":
            graph_y = self.toy_at_stranger
        elif g_sel == "door_open":
            graph_y = self.door_open

        elif g_sel == "toy_tracked":
            graph_y = self.toy_tracked
        elif g_sel == "dog_tracked":
            graph_y = self.dog_tracked
        elif g_sel == "owner_tracked":
            graph_y = self.owner_tracked
        elif g_sel == "stranger_tracked":
            graph_y = self.stranger_tracked

        else:
            self.is_draw_needed = False

        return graph_y


class ECSV:
    def __init__(self, parent):
        self.parent = parent
        self.root = parent.parent
        self.header = []
        self.text = ""
        self.line_length = 0
        self.line_index = 0
        self.length = 0


    def load(self, file_path):
        """Reads the Etho-CSV file at *file_path* and loads config, etc automatically"""

        # Import CSV
        with open(file_path, 'r', 8 << 10) as f:

            # Reading header
            line = header_srt = ""
            while line[0:11] != "Frame,Time,":
                line = f.readline()
                header_srt += line + "\n"

                print(line[0:10])

            self.header_read(header_srt)

            # Init frame with a deepcopy of the current frame and empty self.frames
            frame = self.root.frame

            print("Loading ECSV")
            # Read text
            self.text = f.read()

            # Find out the length of the rows
            first_line = self.text[0:self.text.find('\n')]
            self.line_length = len(first_line) + 1  # Add '\n'
            print("ECSV loaded")


    def header_read(self, header_str):
        """Reads the WHOLE header of the Etho-CSV file and loads config, etc automatically."""

        header_list = header_str.split('\n')
        current_section = ""
        meta_str = config_str = episodes_str = header_str = ""

        # Read the header line by line
        for line in header_list:
            if line != "" and line[0] == '[':
                current_section = line

            # Sort lines by looking at the section tags
            if current_section == "[METADATA]":
                meta_str += line + "\n"
            elif current_section == "[CONFIG]":
                config_str += line + "\n"
            elif current_section == "[EPISODES]":
                episodes_str += line + "\n"
            elif current_section == "[MEASUREMENT]":
                header_str += line + "\n"

        # Load the parameters
        self.root.metadata_read(meta_str)
        self.root.config_read(config_str)
        self.root.episodes_read(episodes_str)
        self.dataheader_read(header_str)


    def dataheader_read(self, header_str):
        """Reads the main part of the Etho-CSV header -> The header of the data"""

        lines = header_str.split('\n')
        self.header = lines[2].split(',')

        print("Header loaded")


    def read_line(self):
        """Reads the indexth line from the data loaded by self.load"""

        line = self.text[self.line_index*self.line_length : (self.line_index+1)*self.line_length - 1]
        cells = line.split(',')

        # Read through cells in the line
        for i in range(len(self.header)):

            if self.header[i] == "Frame":
                self.root.frame.sn = int(cells[i])
            elif self.header[i] == "Time":
                self.root.frame.time = float(cells[i])
            elif self.header[i] == "Episode counter":
                self.root.episode.number = float(cells[i])
                if self.root.episode.number == 0.5:
                    self.root.episode.text.set(self.root.episode.names[0])
                else:
                    self.root.episode.text.set(self.root.episode.names[int(self.root.episode.number-1)])
            elif self.header[i] == "Episode name":
                pass
            else:

                # Now that we are done with the general frame data, let's take care of rigid bodies!
                header_parts = self.header[i].split('.')

                # Find rigid body
                rb = self.root.frame.rigid_body_get(header_parts[0])

                # Basic parameters (pos, rot, tracked)
                if header_parts[1] == "position":
                    if header_parts[2] == "x":
                        rb.position.x = float(cells[i])
                    elif header_parts[2] == "y":
                        rb.position.y = float(cells[i])
                    elif header_parts[2] == "z":
                        rb.position.z = float(cells[i])
                elif header_parts[1] == "rot_Z":
                    rb.rotation = Vector(0, 0, float(cells[i])).to_quaternion(degrees=True)
                elif header_parts[1] == "tracked":
                    rb.tracked = cells[i] == "1"
                elif header_parts[1] == "was_seen":
                    if cells[i] == "1":
                        rb.last_seen = 0
                    else:
                        rb.last_seen = None

                # Extra info
                elif header_parts[1] == "is_in_room":
                    rb.is_in_room = cells[i] == "1"
                elif header_parts[1] == "has_toy":
                    rb.has_toy = cells[i] == "1"
                elif header_parts[1] == "is_open":
                    rb.is_open = cells[i] == "1"

        # Increment the line index
        self.line_index = self.line_index + 1


class EthoFrame(Frame):
    def __init__(self, parent, *args, **keywords):
        super().__init__(*args, **keywords)

        self.parent = parent

        # Clear visibility frame
        if self.parent.frame_check is not None:
            for widget in self.parent.frame_check.winfo_children():
                widget.destroy()

        # Create rigid bodies
        self.rigid_body_add(RigidBody(0, "DOG",  r=0.4,  color="#AA5500", tracked=False))
        self.rigid_body_add(RigidBody(1, "OWN",          color="#11CC11", tracked=False))
        self.rigid_body_add(RigidBody(2, "OHA",  r=0.1,  color="#11CC11", tracked=False))
        self.rigid_body_add(RigidBody(3, "STR",          color="#1111FF", tracked=False))
        self.rigid_body_add(RigidBody(4, "SHA",  r=0.1,  color="#1111FF", tracked=False))
        self.rigid_body_add(RigidBody(5, "TOY",  r=0.07, color="yellow",  tracked=False))
        self.rigid_body_add(RigidBody(6, "DOOR", r=0.1,  color="red",     tracked=False))
        self.rigid_body_add(RigidBody(7, "WAND", r=0.05, color="white",   tracked=False))

        # Create references for easier use
        self.dog = self.rigid_body_get("DOG")
        self.own = self.rigid_body_get("OWN")
        self.oha = self.rigid_body_get("OHA")
        self.str = self.rigid_body_get("STR")
        self.sha = self.rigid_body_get("SHA")
        self.toy = self.rigid_body_get("TOY")
        self.door = self.rigid_body_get("DOOR")
        self.wand = self.rigid_body_get("WAND")

        # Create groups of rigid bodies
        self.participants_is_in_room = [self.own, self.str]
        self.participants_has_toy = [self.dog, self.own, self.str]
        self.participants_has_hand = [self.own, self.str]

        # Add eto variables
        # is_in_room
        for rb in self.participants_is_in_room:
            rb.is_in_room = False

        # has_toy
        for rb in self.participants_has_toy:
            rb.has_toy = False
            if rb.name == "DOG":
                rb.toy_threshold = 0.35
            else:
                rb.toy_threshold = 0.22

        # has hands
        self.own.hand = self.oha
        self.str.hand = self.sha
        # The rest has itself as hand
        for rb in self.participants_has_toy:
            if rb not in self.participants_has_hand:
                rb.hand = rb

        # door
        self.door.x = ROOM_X_WIDTH_M - 1#self.parent.origin_x TODO: That's not how you do it, man
        self.door.len = 1
        self.door.is_open = False
        self.door.was_open = False
        self.door.open_last = 0
        self.door.closed_position = Vector()


    def get_data(self, frame):
        """Updates *self.frame* from *frame*"""

        # Update time
        self.sn = frame.sn
        self.time = frame.time

        # Search for rigid bodies and update if possible
        for rb in self.rigid_bodies:
            dummy = frame.rigid_body_get(rb.name)

            if dummy is not None:
                rb.position =  dummy.position
                rb.rotation =  dummy.rotation
                rb.tracked =   dummy.tracked
                rb.last_seen = dummy.last_seen


class CSV:
    def __init__(self, parent):
        self.parent = parent
        self.read_path = None   # Path of the CSV file that is being read
        self.write_path = ""    # Path of the CSV file that is being written
        # ECSV
        self.is_ecsv = False  # False --> Regular CSV file from MoCap;  True --> ECSV file from EthoLink
        self.ecsv = ECSV(self)
        self.read_thread = None


    def load(self):
        """Opens an Open file dialog then starts a CSV-read or ECSV-read process"""

        # Get file path
        self.read_path = filedialog.askopenfilename(initialdir="", title="Select a .CSV or .ECSV file",
                                                    filetypes=(("All files", "*.*"),
                                                    ("CSV files", "*.csv"), ("Etho-CSV files", "*.ecsv")))
        if self.read_path == "":
            return

        # Reset frame TODO: reset EtoFilter function!!
        self.parent.frame.__init__(self.parent)
        self.parent.episode.reset()
        self.parent.canvas.delete("all")
        self.parent.graph.canvas.delete("all")

        if self.read_path[-5:] == ".ecsv":
            self.is_ecsv = True
            self.ecsv.load(self.read_path)
        else:
            self.is_ecsv = False


    def read_start(self):
        """Starts CSV-reading process in a separate thread"""

        if self.is_ecsv:
            pass
        else:
            lamb = lambda: [self.parent.mocap.csv_read(self.read_path, 10)]
            self.read_thread = Thread(target=lamb)
            self.read_thread.start()


    def read_stop(self):
        """Stops CSV-reading process"""

        self.parent.mocap.csv_stop()
        self.read_thread.join()

    # TODO: Put this into the ECSV module
    def write_header(self):
        """Writes header of the CSV file"""

        # Create file path TODO: add date
        str_date = getDate()

        current_dir = os.path.dirname(__file__)
        self.write_path = os.path.join(current_dir, "measurements", self.parent.measurement_name + "_" + str_date + ".ecsv")

        # Header header
        content = "### Preprocessed Eto-measurement CSV-file ###\n\n"

        # TODO: Write metadata

        # Write config
        content += "[CONFIG]\n"
        content += self.parent.config_build()
        content += "\n"

        # Write episodes
        content += "[EPISODES]\n"
        content += self.parent.episodes_build()
        content += "\n\n"

        # Write header
        # Frame
        content += "[MEASUREMENT]\nFrame,Time,Episode counter,"#Episode name,"
        # Rigid bodies
        for rb in self.parent.frame.rigid_bodies:
            # Position
            content += rb.name + ".position.x," + rb.name + ".position.y," + rb.name + ".position.z,"
            # Rotation
            content += rb.name + ".rot_Z,"
            # Tracked
            content += rb.name + ".tracked,"
            # Was seen
            content += rb.name + ".was_seen,"
            # in_room
            if rb in self.parent.frame.participants_is_in_room:
                content += rb.name + ".is_in_room,"
            # has_toy
            if rb in self.parent.frame.participants_has_toy:
                content += rb.name + ".has_toy,"
            # is_open
            if rb.name == "DOOR":
                content += rb.name + ".is_open,"

        # Remove last comma
        content = content[0:-1]

        # Write file
        with open(self.write_path, 'w') as f:
            f.write(content)


    def write_line(self):
        """Writes a row/frame of the CSV-file"""

        # If there is no CSV to write in, return
        if self.write_path == "":
            return

        # Formatting
        f_framenum = "%6i"
        f_time = "%8.3f"
        f_episode = "%4.1f"
        f_pos = "% 10.5f"
        f_bool = "%i"

        # Frame
        content = "\n" + f_framenum % self.parent.frame.sn + "," + f_time % self.parent.frame.time + "," + \
            f_episode % self.parent.episode.number + ","# + self.episode.text.get() + ","
        # Rigid bodies
        for rb in self.parent.frame.rigid_bodies:
            # Position
            content += f_pos % rb.position.x + "," + f_pos % rb.position.y + "," + f_pos % rb.position.z + ","
            # Rotation
            content += f_pos % rb.rotation.project("Z") + ","
            # Tracked
            content += f_bool % rb.tracked + ","
            # Was seen
            content += f_bool % (rb.last_seen is not None) + ","
            # is_in_room
            if rb in self.parent.frame.participants_is_in_room:
                content += f_bool % rb.is_in_room + ","
            # has_ball
            if rb in self.parent.frame.participants_has_toy:
                content += f_bool % rb.has_toy + ","
            # is_open
            if rb.name == "DOOR":
                content += f_bool % rb.is_open + ","

        # Remove last comma
        content = content[0:-1]

        # Write file
        with open(self.write_path, 'a') as f:
            f.write(content)


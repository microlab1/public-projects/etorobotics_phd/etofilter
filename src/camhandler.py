import cv2
import time
from threading import Thread

# Mennyire pontos a BaslerCam időzítése?
# Másik opció: a recording után visszaolvassuk és utólag pontosítunk az FPS-en (framecount_prop / elapsed time)

class CamHandler:
    def __init__(self, saveName="UNNAMED", vidStrVar=None):
        # Video
        self.vidCap = cv2.VideoCapture(1)
        self.vidStrVar = vidStrVar

        # Properties
        self.fps = int(self.vidCap.get(cv2.CAP_PROP_FPS))
        self.size = (int(self.vidCap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self.vidCap.get(cv2.CAP_PROP_FRAME_HEIGHT)))

        # Recording
        self.saveName = saveName
        vidType = cv2.VideoWriter_fourcc(*'XVID')
        self.vidwrite = cv2.VideoWriter(saveName + ".avi", vidType, self.fps, self.size)

        # Flags
        self.recording = False
        self.terminate = True
        self.camThread = None


    def __videoOut(self):
        ret = True
        while ret and not self.terminate:
            # Read frame
            ret, frame = self.vidCap.read()

            # Record frame if needed
            if self.recording and ret:
                self.vidwrite.write(frame)

            # Add text to frame
            #if self.vidStrVar is not None:
            #    frame = cv2.putText(frame, self.vidStrVar.get(),
            #                        (10, 50), cv2.FONT_HERSHEY_PLAIN, 1, (225, 225, 225), 2, cv2.LINE_AA, False)

            # Display frame
            cv2.imshow("Camera", frame)

            # Quit on pressing Q
            if cv2.waitKey(1) == ord('q'):
                break

    def start(self):
        self.terminate = False
        self.camThread = Thread(target=self.__videoOut)
        self.camThread.start()
        print("Camera thread started")

    def stop(self):
        # If there is an ongoing recording, stop it
        if self.recording:
            self.recStop()

        self.terminate = True
        self.camThread.join()
        self.vidCap.release()
        self.vidwrite.release()
        cv2.destroyAllWindows()
        print("Camera thread joined")

    def recStart(self):
        if not self.terminate:
            #with open(self.saveName + "master.csv", "a") as f:
            #    f.write("camstart," + str(time.time())+'\n')
            self.recording = True
            print("RECORDING")

    def recStop(self):
        if not self.terminate:
            #with open(self.saveName + "master.csv", "a") as f:
            #    f.write("camend," + str(time.time())+'\n')
            self.recording = False
            print("Recording over")

    def setName(self, saveName):
        """Sets the save name of the recordings"""
        self.saveName = saveName
        vidType = cv2.VideoWriter_fourcc(*'XVID')
        self.vidwrite = cv2.VideoWriter(saveName + ".avi", vidType, self.fps, self.size)



if __name__ == "__main__":

    cam = CamHandler("CAMTEST")
    cam.start()

    time.sleep(1)
    cam.recStart()

    time.sleep(5)
    cam.recStop()

    time.sleep(1)
    cam.stop()

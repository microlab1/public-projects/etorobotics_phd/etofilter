from tkinter import *

# Colors
c_back = '#333333'
c_butt = '#252525'
c_clck = '#424242'
c_text = '#AAAAAA'
c_stop = '#551111'
c_play = '#115511'
c_green = '#115511'


def ui_init(root, frame_mode, frame_ip, frame_open, frame_stream):
    root.title('Chiasma Opticum')
    root.resizable(0, 0)
    root.configure(bg=c_back)

    # Title

    DarkLabel(frame_mode, text="MoCap to", font=("Arial Bold", 13)).grid(row=0, column=0)
    root.iconbitmap('m2m.ico')

    # IPs
    DarkLabel(frame_ip, text="Server IP address:   ").grid(row=0, column=0, sticky="W")
    DarkLabel(frame_ip, text="Eto IP address:   ").grid(row=1, column=0, sticky="W")
    DarkLabel(frame_ip, text="     Port:   ").grid(row=1, column=2, sticky="W")

    # Descriptions
    DarkLabel(frame_open, text="   Stream from CSV file reading every ").grid(row=0, column=2)
    DarkLabel(frame_open, text="th line.").grid(row=0, column=4)
    DarkLabel(frame_stream, text="   Stream from MoCap stream").grid(row=0, column=2)


class DarkButton(Button):
    def __init__(self, frame, text, lambda_default, lambda_active=None, width=20, pady=5,
                 text_active="Stop", color=c_butt, color_active=c_stop, color_text=c_text, color_text_active=c_text, *args, **keywords):

        super().__init__(frame, text=text, width=width, pady=pady, bd=0,
                         fg=color_text, bg=c_butt, activebackground=c_clck, activeforeground=color_text_active,
                         command=lambda_default, *args, **keywords)
        self.active = False

        if lambda_active is not None:
            self.lambda_default = lambda: [lambda_default(), self.on()]
            self.lambda_active = lambda: [lambda_active(), self.off()]
            self.configure(command=self.lambda_default)
        else:
            self.lambda_default = lambda_default
            self.lambda_active = lambda_active

        self.text = text
        self.text_active = text_active
        self.color = color
        self.color_active = color_active
        self.color_text = color_text
        self.color_text_active = color_text_active
        self.other_buttons = []

    def on(self):
        self.configure(bg=self.color_active, text=self.text_active, command=self.lambda_active, fg=self.color_text_active)
        self.active = True
        for ob in self.other_buttons:
            ob.configure(state=DISABLED)

    def off(self):
        self.configure(bg=self.color, text=self.text, command=self.lambda_default, fg=self.color_text)
        self.active = False
        for ob in self.other_buttons:
            ob.configure(state=NORMAL)

    def update_lambdas(self, lambda_default, lambda_active=None):
        self.lambda_default = lambda_default
        self.lambda_active = lambda_active
        self.configure(command=self.lambda_default)


class DarkRadioButton(Radiobutton):
    def __init__(self, frame, variable, value, text, font=("Arial Bold", 13), *args, **keywords):
        super().__init__(frame, text=text, indicatoron=False, variable=variable, value=value,
                         activeforeground=c_text, activebackground=c_butt, selectcolor=c_clck, fg=c_text,
                         bg=c_butt,
                         borderwidth=0, font=font, *args, **keywords)

        variable.trace_add("write", self.text_color)
        self.variable = variable
        self.value = value

        self.text_color()

    def text_color(self, *args, **keywords):
        if self.variable.get() == self.value:
            self.configure(fg=c_text, activeforeground=c_text)
        else:
            self.configure(fg=c_clck, activeforeground=c_clck)


class DarkEntry(Entry):
    def __init__(self, frame, width=7, init=""):
        super().__init__(frame, width=width, bg=c_butt, fg=c_text, bd=0,
                         highlightthickness=1, highlightbackground="grey10", highlightcolor="grey10")

        self.insert(END, init)
        self.is_valid = True

    def get_ip(self, default="127.0.0.1"):
        # Get IP
        ip = self.get()
        ip_list = ip.split('.')

        # IP addresses must have 4 parts
        if len(ip_list) != 4:
            self.invalid()
            return default

        for ip_part in ip_list:
            try:
                num = int(ip_part)
                if num < 0 or num > 255:
                    self.invalid()
                    return default
            except:
                self.invalid()
                return default

        # If there's no problem with the IP address, return that
        self.valid()
        return ip

    def get_number(self, default=0, usedefault=True, integer=False, nonnegative=False, nonzero=False, canbeempty=False):
        # Get number
        num = self.get()

        # If empty string is allowed, return None
        if canbeempty and num == "":
            self.valid()
            return None

        try:
            num = float(num)

            # Test restrictions
            if integer and round(num) != num \
                    or nonzero and num == 0 \
                    or nonnegative and num < 0:
                self.invalid()
                if usedefault:
                    return default
                else:
                    return None

            if integer:
                num = int(num)

        # Error
        except:
            self.invalid()
            if usedefault:
                return default
            else:
                return None

        # If there's no problem with the number, return that
        self.valid()
        return num

    def invalid(self):
        self.configure(highlightbackground="red", highlightcolor="red")
        self.is_valid = False

    def valid(self):
        self.configure(highlightbackground="grey10", highlightcolor="grey10")
        self.is_valid = True

    def set(self, text):
        self.delete(0, END)
        self.insert(0, text)

    def is_empty(self):
        if self.get() == "":
            return True
        else:
            return False


class DarkLabel(Label):
    def __init__(self, frame, text, *args, **keywords):
        super().__init__(frame, text=text, fg=c_text, bg=c_back, *args, **keywords)


class DarkLabelFrame(LabelFrame):
    def __init__(self, frame, *args, **keywords):
        super().__init__(frame, bd=0, fg=c_text, bg=c_back, *args, **keywords)
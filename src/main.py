#from threads import *
from threading import Thread
from gui_dark import *
import socket
from EthoLink import EthoLink
from EthoModules import *
from mocapy.src.mocapy import MoCap
from queue import Queue
import time

# TODO: max_fps helyett "realtime" próbálni a frame olvasást
# ============================= INIT =============================

root = Tk()
root.protocol("WM_DELETE_WINDOW", lambda: root_exit())

# ============================= THREADS =============================

t_csv = t_eto = t_stream = None

# ============================= GUI =============================

# Frames
#frame_mode = DarkLabelFrame(root, padx=17, pady=10)
#frame_ip = DarkLabelFrame(root, padx=17, pady=10)
#frame_open = DarkLabelFrame(root, padx=17, pady=0)
#frame_stream = DarkLabelFrame(root, padx=17, pady=17)

# Init
#ui_init(root, frame_mode, frame_ip, frame_open, frame_stream)

# Radio buttons
#mode = IntVar()
#mode.set(1)

#radio_matlab = DarkRadioButton(frame_mode, mode, 1, " Matlab ")
#radio_dda = DarkRadioButton(frame_mode, mode, 2, " DDA ")
#radio_deep = DarkRadioButton(frame_mode, mode, 3, " Deep learning ")

# Action buttons
#button_open = DarkButton(frame_open, "From CSV", lambda: csv_init(), lambda: Thread(target=lambda: csv_stop()).start())
#button_stream = DarkButton(frame_stream, "From MoCap stream", lambda: stream_init(), lambda: Thread(target=lambda: stream_stop()).start())

#button_open.other_buttons = [button_stream]
#button_stream.other_buttons = [button_open]

# Entries
#entry_server_ip = DarkEntry(frame_ip, width=16, init="192.168.100.105")
#entry_send_ip = DarkEntry(frame_ip, width=16, init="127.0.0.1")
#entry_send_port = DarkEntry(frame_ip, init="24123")
#entry_every_xth = DarkEntry(frame_open, width=3, init="5")

# Organize
#frame_mode.pack(anchor=W)
#frame_ip.pack(anchor=W)
#frame_open.pack(anchor=W)
#frame_stream.pack(anchor=W)
#radio_matlab.grid(row=0, column=1)
#radio_dda.grid(row=0, column=2)
#radio_deep.grid(row=0, column=3)
#entry_server_ip.grid(row=0, column=1, sticky="W")
#entry_send_ip.grid(row=1, column=1, sticky="W")
#entry_send_port.grid(row=1, column=3, sticky="W")
#button_open.grid(row=0, column=0)
#entry_every_xth.grid(row=0, column=3)
#button_stream.grid(row=0, column=0)

# ============================= UDP INIT =============================

eto_address = ("127.0.0.1", 24123)
nnc_address = ("127.0.0.1", 1511)
m2m_address = ("127.0.0.1", 5005)
sckt = udp_init(m2m_address)

# ============================= MOCAP =============================

# Create data que
queue = Queue()

# Create MoCap
mocap = MoCap("192.168.100.105", queue=queue)

# Create EtoFilter
eto = EthoLink(root=root, queue=queue, mocap=mocap, socket=sckt, address=eto_address, origin_x=1, origin_y=1)

# ============================= BUTTON FUNCTIONS =============================


def stream_init():
    # New window
    eto.new_window("MoCap stream")

    # Set IP addresses
    mocap.server_address_set(entry_server_ip.get_ip(default="192.168.100.105"))
    eto.address = (entry_send_ip.get_ip(), entry_send_port.get_number(integer=True, nonnegative=True))

    # Start threads
    mocap.natnet_start()
    global t_eto
    t_eto = ThreadWithTrace(target=lambda: [eto.stream_from_queue(20)])
    t_eto.start()


def stream_stop():
    mocap.natnet_stop()
    eto.process_frames_stop()

    # Clear queue
    while not mocap.queue.empty():
        mocap.queue.get()

    # Join thread
    global t_eto
    t_eto.join()

    # Close eto window
    eto.root.destroy()


def root_exit():
    if button_open.active:
        Thread(target=lambda: csv_stop()).start()
    if button_stream.active:
        Thread(target=lambda: stream_stop()).start()

    while t_eto.is_alive():
        time.sleep(0.01)
        root.update()

    sckt.shutdown(socket.SHUT_WR)
    root.destroy()

# ============================= LOOP =============================

# Loop it so it wont insta-die
root.mainloop()
